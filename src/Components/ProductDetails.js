import React, { Component } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { ProductContext } from './ProductContext';

function ProductDetails() {
  const { id } = useParams();

  return (
    <div className='product-details'>
      <ProductContext.Consumer>
        {
          (productContextProps) => {
            const product = productContextProps.productList[id];
            if (product)
              return (
                <>
                  <h1>{product.name}</h1>
                  <div className='product-image'>
                    <img src={product.imgLink} />
                  </div>
                  <div className='product-description'>
                    <p>{product.description}</p>
                  </div>
                </>
              )
            else
              return null
          }
        }
      </ProductContext.Consumer>
    </div>
  )
}

export default ProductDetails





